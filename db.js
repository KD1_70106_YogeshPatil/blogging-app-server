const mysql = require('mysql2')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'blogging_db',
  port: 3306,
})

module.exports = {
  pool,
}