
const express = require('express')
const userRouter = require('./routes/user')
const blogRouter = require('./routes/blog')

const app = express()
app.use(express.json())

// adding routes
app.use('/user', userRouter)
app.use('/blog', blogRouter)

// this is updated by second developer
app.listen(4000, '0.0.0.0', () => {
  console.log(`server started successfully on port 4000`)
})