const express = require('express')
const db = require('../db')

const router = express.Router()

router.get('/', (request, response) => {
  const query = 'select id, title, userId, contents from blogs;'
  db.pool.execute(query, (error, result) => {
    response.send({ error, result })
  })
})

router.post('/', (request, response) => {
  const { title, user, contents } = request.body
  const query = 'insert into blogs (title, userId, contents) values (?, ?, ?)'
  db.pool.execute(query, [title, user, contents], (error, result) => {
    response.send({ error, result })
  })
})

router.delete('/:id', (request, response) => {
  const { id } = request.params
  const query = 'delete from blogs where id = ?'
  db.pool.execute(query, [id], (error, result) => {
    response.send({ error, result })
  })
})

module.exports = router
