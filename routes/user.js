const express = require('express')
const cryptoJs = require('crypto-js')

const db = require('../db')

const router = express.Router()

router.post('/signup', (request, response) => {
  const { name, email, password } = request.body
  const encryptedPassword = String(cryptoJs.MD5(password))
  const query = `insert into user (name, email, password) values (?, ?, ?)`
  db.pool.execute(query, [name, email, encryptedPassword], (error, result) => {
    response.send({ error, result })
  })
})

router.get('/', (request, response) => {
    const query = 'select id, name, email, password from user;'
    db.pool.execute(query, (error, result) => {
      response.send({ error, result })
    })
  })
router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const query = `select * from user where email = ? and password = ?`

  const encryptedPassword = String(cryptoJs.MD5(password))
  db.pool.execute(query, [email, encryptedPassword], (error, users) => {
    if (users.length == 0) {
      response.send({ status: 'error', result: 'invalid email or password' })
    } else {
      response.send({ error, user: users[0] })
    }
  })
})

module.exports = router
